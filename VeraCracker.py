#!/usr/bin/env python3
# -*- coding: utf8 -*-
#
# VeraCrypt volume password cracker
#
# Copyright (c) 2015   NorthernSec
# Copyright (c) 2015   Pieter-Jan Moreels
# This software is licensed under the Original BSD License

from datetime import datetime
import subprocess
import platform
import argparse
import codecs
import time
import sys
import os

class bcolors:
	DARKPURPLE = "\033[1;35m"
	DARKGREEN  = '\033[92m'
	YELLOW     = '\033[93m'
	RED        = '\033[91m'
	ENDC       = "\033[1;m"
	BOLD       = '\033[1m'
	# Bold coloring
	BOLDGREEN = BOLD+DARKGREEN
	BOLDRED   = BOLD + RED

def print_info(message):
    print((bcolors.RED) + ("[*] ") + (bcolors.YELLOW) + (str(message)))

def print_error(message):
    print((bcolors.DARKGREEN) + ("[*] ") + (bcolors.RED) + (str(message)) + (bcolors.ENDC))


# Global variables to use later on
# MacOS
VeraMacPath = '/Applications/VeraCrypt.app/Contents/MacOS/VeraCrypt'
# Linux Os
VeraLinuxPath = 'veracrypt'
VeraLinuxAttributes = ' -t %s -p "%s" --non-interactive'

# Version
version = 'v0.2'

# Debug mode on by default
global debug
debug = 'on'

# Clear terminal
os.system("clear")

# There always has to be a benner dudes
def banner():
    print(bcolors.RED + """
'##::::'##:'########:'########:::::'###:::::'######::'########:::::'###:::::'######::'##:::'##:'########:'########::
 ##:::: ##: ##.....:: ##.... ##:::'## ##:::'##... ##: ##.... ##:::'## ##:::'##... ##: ##::'##:: ##.....:: ##.... ##:
 ##:::: ##: ##::::::: ##:::: ##::'##:. ##:: ##:::..:: ##:::: ##::'##:. ##:: ##:::..:: ##:'##::: ##::::::: ##:::: ##:
 ##:::: ##: ######::: ########::'##:::. ##: ##::::::: ########::'##:::. ##: ##::::::: #####:::: ######::: ########::
. ##:: ##:: ##...:::: ##.. ##::: #########: ##::::::: ##.. ##::: #########: ##::::::: ##. ##::: ##...:::: ##.. ##:::
:. ## ##::: ##::::::: ##::. ##:: ##.... ##: ##::: ##: ##::. ##:: ##.... ##: ##::: ##: ##:. ##:: ##::::::: ##::. ##::
::. ###:::: ########: ##:::. ##: ##:::: ##:. ######:: ##:::. ##: ##:::: ##:. ######:: ##::. ##: ########: ##:::. ##:
:::...:::::........::..:::::..::..:::::..:::......:::..:::::..::..:::::..:::......:::..::::..::........::..:::::..::
    """ + bcolors.ENDC)
    print("""
    Version   : %s
    """ % str(version))

# Checks if veracrypt is running (Windows function)
def isVeraRunning():
    return True if ''.join(os.popen(VeraWinProcList).readlines()).find(VeraWinProcName) >= 0 else False

# Add a progress bar for when cracking
def progressbar(it, prefix="Cracking ", size=50):
    global debug
    if debug == 'on':
        pass

    count = len(it)

    def _show(_i):
        if count != 0 and sys.stdout.isatty():
            x = int(size * _i / count)
            sys.stdout.write("%s[%s%s] %i/%i\r" % (prefix, "#" * x, " " * (size - x), _i, count))
            sys.stdout.flush()
    _show(0)
    for i, item in enumerate(it):
        yield item
        _show(i + 1)
    sys.stdout.write("\n")
    sys.stdout.flush()

# Error handling in Linux
def linuxCrack(p, veracryptPath):
    cmd = veracryptPath + VeraLinuxAttributes % (args.v, p)
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()

    procreturn = str(out, "utf-8").strip() if out else str(err, "utf-8").strip()

    if procreturn.find("Error: Operation failed due to one or more of the following:") != -1:
        return False
        sys.exit()
    elif procreturn == "Error: Failed to obtain administrator privileges.":
        print_error("This script requires root previleges")
        sys.exit()
    elif procreturn.find("No such file or directory") != -1:
        print_error("VeraCrypt cannot be found: Install VeraCrypt or use the -b argument")
        sys.exit()
    elif procreturn == "Error: The volume you are trying to mount is already mounted.":
        print_info("This volume is already mounted")
        print('Please unmount the drive/volume you are trying to crack')
        sys.exit()
    else:
        print_error(procreturn)
        return True


def printResults(startTime, tried):
    print("Cracking duration: %s" % str(datetime.now() - startTime))
    print("Averange time per try: %s" % str((datetime.now() - startTime) / tried))
    print("Passwords tried:   %s" % tried)


if __name__ == '__main__':
    banner()

    # Parse arguments
    description = '''VeraCrypt volume cracker'''

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-v', metavar='volume', type=str, required=True, help='Path to volume')
    parser.add_argument('-p', metavar='list', type=str, nargs="?", help='Password list')
    parser.add_argument('-d', action='store_true', help='Debug mode (default: enabled)')
    parser.add_argument('--no-debug', action='store_true', help='No debugging')
    parser.add_argument('-o', metavar='file', type=str, help='Output file for untried passwords on quit')
    parser.add_argument('-b', metavar='file', type=str, help='Path to the VeraCrypt binary')
    parser.add_argument('-f', action='store_true', help='Unmount Mountpoint before execution')
    args = parser.parse_args()

    if args.no_debug:
        debug = 'off'

    # Get VeraCypt binary path
    if platform.system() == "Linux":
        veracryptPath = VeraLinuxPath
        crack = linuxCrack
    elif platform.system() == "Darwin":
        veracryptPath = VeraMacPath
        crack = linuxCrack
    else:
        print_error("This program has not yet been developed for " + os.name)
        sys.exit()

    if args.b:
        veracryptPath = args.b

    # Grab the wordlist
    wordlist = [x.strip() for x in open(args.p, 'r') if x.strip()] if args.p else [line.strip() for line in fileinput.input()]

    # Time to test
    wlCopy = list(wordlist)
    tried = 0
    startTime = datetime.now()
    try:
        for p in progressbar(wordlist):
            tried += 1
            if args.d or debug == 'on':
                print("[-] Trying %s" % p)
            if crack(p, veracryptPath):
                print("[+] Password found! [ " + bcolors.DARKGREEN + p + bcolors.ENDC + " ]")
                printResults(startTime, tried)
                sys.exit(0)
            wlCopy.pop(0)
        print_info("Password not found")
        printResults(startTime, tried)
    except KeyboardInterrupt:
        print_error("[!] VeraCrack interrupted")
        if args.o:
            if args.d:
                print("[-] Saving leftover passwords to %s" % args.o)
            f = open(args.o, 'w')
            f.write("\n".join(wlCopy))
