# VeraCracker
Veracrypt Password Cracker <br />
This script will go through a list of passwords and try these against the specified volume.
If one of the passwords is a match, it will automatically mount the volume
(for now only auto-mounts on external devices)
This works both on volumes and hard drives such as USB, external hard drives, etc.

### Notes
VeraCracker only works with **Python 3.x** version.
Works on Linux. On Windows, it doesn't seem to work. I have removed all the
Windows functions and currently working on that.

Contributions are welcomed.

If you are running MacOS, please test it and let me know if it works or not.

## Disclaimer
This tool is for educational purposes only and is not intended to be put into
practise unless you have authorised access to the system you are trying to break into.

## License

GNU GENERAL PUBLIC LICENSE V3
